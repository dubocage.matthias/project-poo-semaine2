<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Upload;
use App\Form\UploadType;
use App\Entity\ParserWasteJson;

class UploadController extends AbstractController
{
    /**
     * @Route("/upload", name="upload")
     */
    public function index(Request $request) {
        
        $error = '';
        $upload = new Upload();
        $form = $this->createForm(UploadType::class , $upload);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $file = $upload->getName();

            if(strpos($file->getClientOriginalName(),'.json')){

                $file->move($this->getParameter('upload_directory') , 'data.json');
                $parser = new ParserWasteJson($this->getParameter('upload_directory').'/data.json');
                $date = new \DateTime();
                $parser->generateRelustatFile($this->getParameter('upload_directory').'/traiter/data'.$date->format('YmdHi').'.json',$this->getParameter('upload_directory').'/data.json');

                unlink($this->getParameter('upload_directory'). '/data.json');
 
                return $this->redirectToRoute('traiter');

            }else{

                $error = "Ce n'est pas un fichier JSON !";

            }
        }

        return $this->render('upload/index.html.twig', [
            'form' => $form->createView() , 'error' => $error
        ]);

    }

    /**
     * @Route("/traiter", name="traiter")
     */
    public function traiter(){

        $files = array_diff(scandir($this->getParameter('upload_directory').'/traiter'), array('..', '.'));

        $co2s = [];

        foreach($files as $file){

            $data = file_get_contents($this->getParameter('upload_directory').'/traiter/'.$file);
            array_push($co2s, json_decode($data));

        }

        $result = [];
        foreach ($co2s as $key => $co2){
            foreach($co2 as $key2 => $district){
                $result[$key][$key2] = $district;
            }
        }

        return $this->render('upload/resultat.html.twig', [
            'co2s' => $result
        ]);

    }
}
