<?php


namespace App\Entity\AbstractClass;

use App\Entity\Waste;

abstract class Service{

    /**
     * @var float
     */
    protected $capacity;

    /**
     * @param Waste $waste
     * @return float
     */
    abstract protected function wasteTreatment(Waste $waste) : float;
    
    /**
     * @param Waste $waste
     * @return bool
     */
    abstract protected function wasteAccept(Waste $waste) : bool;

    /**
     * @return bool
     */
    public function isClose() : bool{
        
        return $this->capacity == 0;

    } 

    /**
     * @return float
     */
    public function getCapacity() : float{

        return $this->capacity;

    }

}