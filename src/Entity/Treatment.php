<?php

namespace App\Entity;

use App\Entity\District;
use App\Entity\Service;
use App\Entity\Co2Data;

class Treatment{

    /**
     * @var District[]
     */
    private $districts;

    /**
     * @var Service[]
     */
    private $services;

    /**
     * @param District[]
     * @param Services[]
     * 
     */
    public function __construct( $districts , $services ) {

         $this->districts = $districts;
         $this->services = $services;

    }
 
    /**
     * @return Co2Data
     */
    public function run() {

        $co2 = new Co2Data($this->districts); 

        foreach($this->districts as  $district){
            foreach($district->getWastes() as $waste){
                while (!$waste->isTreated()){
                    foreach($this->services as $service){
                        if($service->wasteAccept($waste) && !$service->isClose()){
                            $co2On = $service->wasteTreatment($waste);
                            $co2->addCo2OnDistrict($district->getName(),$co2On);
                        }
                    }
                }
            }
        }

        return $co2;

    } 

    /**
     * @return District[]
     */
    public function getDistrict() {

        return $this->districts;

    }

    /**
     * @return Service[]
     */
    public function getServices() {

        return $this->services;

    }

}