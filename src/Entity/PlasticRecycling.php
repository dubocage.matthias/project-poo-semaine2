<?php

namespace App\Entity;

use App\Entity\AbstractClass\Service;

class PlasticRecycling extends Service{
    
    /**
     * @var array $TABCO2
     */
    private static $TABCO2 = ['PET' => 8, 'PVC' => 12 , 'PC' => 10, 'PEHD' => 11];

    /**
     * @var string[] $tabPlasticAccept
     */
    private $tabPlasticAccept = [];

    /**
     * @param float $capacity
     * @param string[] $tab
     */
    public function __construct(float $capacity, array $tab){

        $this->capacity = $capacity;
        $this->tabPlasticAccept = $tab;

    }

    /**
     * @param Waste $waste
     * @return float
     */
    public function wasteTreatment(Waste $waste): float{

        $co2= 0.0 ;

        if($this->capacity >= $waste->getKg()){

            $co2 = $waste->getKg()*$this::$TABCO2[$waste->getType()];
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());

        }else{

            $co2 = $this->capacity*$this::$TABCO2[$waste->getType()];
            $waste->removeKg($this->capacity);
            $this->capacity = 0;

        }

        return $co2;

    }

    /**
     * @param Waste $waste
     * @return bool
     */
    public function wasteAccept(Waste $waste) : bool{

        foreach($this->tabPlasticAccept as $accept)
            if( $accept == $waste->getType())
                return true;

        return false;
        
    }
}