<?php

namespace App\Entity;

class District{

    /**
     * @var int
     */
    private $population;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Waste[]
     */
    private $wastes;

    /**
     * @param int $population
     * @param Waste[] $wastes
     * @param string $name
     */
    public function __construct(int $population , array $wastes, string $name){
        
        $this->population = $population;
        $this->wastes = $wastes;
        $this->name = $name;

    }

    /**
     * @return Waste[]
     */
    public function getWastes() : array {

        return $this->wastes;

    }

    /**
     * @return int
     */
    public function getPopulation() : int {

        return $this->population;

    }
    
    /**
     * @return string
     */
    public function getName() : string {

        return $this->name;

    }
}