<?php

namespace App\Entity;

use App\Entity\District;

class Co2Data{

    /**
     * @var array 
     */ 
    private $co2ByDistrict;

    /**
     * @param District[] $district
     */
    public function __construct(array $district) {

        foreach($district as $district){

            $this->co2ByDistrict[$district->getName()] = 0;

        }

    }

    /**
     * @param string $nameDistrict
     * @param float $co2
     * @return void
     */
    public function addCo2OnDistrict(string $nameDistrict, float $co2) : void{

        $this->co2ByDistrict[$nameDistrict] += $co2;

    }

    /**
     * @param string $nameDistrict
     * @return float
     */
    public function getCo2OnDistrict(string $nameDistrict) : float {

        return $this->co2ByDistrict[$nameDistrict];
        
    }

    /**
     * @return int
     */
    public function getNbData() {

        return count($this->co2ByDistrict);

    }

    /**
     * @return array
     */
    public function getAllCo2() {

        return $this->co2ByDistrict;

    }

}