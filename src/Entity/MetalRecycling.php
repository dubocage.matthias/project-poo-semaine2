<?php

namespace App\Entity;

use App\Entity\AbstractClass\Service;

class MetalRecycling extends Service {
    
    /**
     * @param float $capacity
     */
    public function __construct(float $capacity){

        $this->capacity = $capacity;

    }

    /**
     * @param Waste $waste
     * @return float
     */
    public function wasteTreatment(Waste $waste): float{

        $co2= 0.0 ;

        if($this->capacity >= $waste->getKg()){

            $co2 = $waste->getKg()*7;
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());

        }else{

            $co2 = $this->capacity*7;
            $waste->removeKg($this->capacity);
            $this->capacity = 0;

        }
        
        return $co2;

    }

    /**
     * @param Waste $waste
     * @return bool
     */
    public function wasteAccept(Waste $waste) : bool{
        
        if( 'metaux' == $waste->getType())
            return true;

        return false;

    }

}