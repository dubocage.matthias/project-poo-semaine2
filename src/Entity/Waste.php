<?php

namespace App\Entity;

class Waste{

    /**
     * @var float
     */
    private $kg;
    /**
     * @var string
     */
    private $type;

    /**
     * @param float $kg
     * @param string $type
     */
    public function __construct(float $kg, string $type){

        $this->kg = $kg;
        $this->type = $type;

    }

    /**
     * @param float $kg
     * @return void
     */
    public function removeKg(float $kg) : void{

        $this->kg -= $kg;

    }

    /**
     * @return float
     */
    public function getKg() : float{

        return $this->kg;

    }

    /**
     * @return string
     */
    public function getType() : string{

        return $this->type;

    }

    /**
     * @return bool
     */
    public function isTreated() : bool{

        return $this->kg == 0;
        
    }
}