<?php

namespace App\Entity;

class ParserWasteJson {

    /**
     * @var mixed
     */
    private $data;

    /**
     * @param string $path
     */
    public function __construct(string $path){

        $jsondata = file_get_contents($path);
        $this->data = json_decode($jsondata);

    }

    /**
     * @return Service[]
     */
    public function generateService() : array {

        $services = [];

        foreach($this->data->{'services'} as $service){

            $nameService = "";
            $capacity = 1;
            $plastic = [];

            foreach($service as $key2 => $data){

                if($key2 == "type")
                    $nameService = $data;
                if($key2 == "ligneFour" || $key2 == "foyers")
                    $capacity *= $data;
                if($key2 == "capacite" || $key2 == "capaciteLigne")
                    $capacity *= $data;
                if($key2 == "plastiques")
                    $plastic = $data;

            }

            switch ($nameService) {

                case "composteur":
                    array_unshift($services , new Composter($capacity));
                    break;

                case "recyclagePapier":
                    array_unshift($services , new PaperRecycling($capacity));
                    break;

                case "recyclageMetaux":
                    array_unshift($services , new MetalRecycling($capacity));
                    break;

                case "recyclageVerre":
                    array_unshift($services , new GlassRecycling($capacity));
                    break;

                case "incinerateur":
                    array_push($services , new Incinerator($capacity));
                    break;

                case "recyclagePlastique":
                    array_unshift($services ,  new PlasticRecycling($capacity, $plastic));
                    break;
                    
                case "centreTri":
                    break;

            }

        }

        return $services;
        
    }

    /**
     * @return District[]
     */
    public function generateDistrict() : array {

        $districts  = [];

        foreach($this->data->{'quartiers'} as $key1 => $quartier){

            $wastes = [];
            $population = 0;

            foreach($quartier as $key2 => $data){

                if($key2 === "population"){

                    $population = $data;

                }else{

                    if(is_object($data)){

                        foreach($data as $key3 => $data2){

                            array_push($wastes,new Waste($data2,$key3));

                        }   

                    }else{

                        array_push($wastes,new Waste($data,$key2));

                    }
                }
            }

            array_push($districts,new District($population , $wastes , $key1));

        }

        return $districts;

    }


    /**
     * @param string $path
     */
    public function generateRelustatFile(string $pathON , string $pathIN) {

        $treated = new Treatment($this->generateDistrict(),$this->generateService());
        $co2 = $treated->run();
        $monfichier = fopen($pathON, 'w+');

        fputs($monfichier,"{ \n");

        foreach( $co2->getAllCo2() as $key => $district ){

            if($key !== count($co2->getAllCo2())-1){

                fputs($monfichier,"\"District ".($key + 1)."\":".$district.",\n");

            }else{

                fputs($monfichier,"\"District ".($key + 1)."\":".$district."\n");

            }

        }

        fputs($monfichier,'}');
        fclose($monfichier);
    }

}