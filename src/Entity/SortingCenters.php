<?php


namespace App\Entity;

use App\Entity\AbstractClass\Service;

class SortingCenters extends Service{
    
    /**
     * @var Service[]
     */
    private $services;

    /**
     * @param float $capacity
     */
    public function __construct(float $capacity){

        $this->capacity = $capacity;

    }

    /**
     * @param Service[]
     * @return void
     */
    public function addServices(array $services) : void{

        $this->services = $services;

    }

    /**
     * @param Waste $waste
     * @return float
     */
    public function wasteTreatment(Waste $waste): float{

        $co2 = 0.0;

        if($waste->getKg() <= $this->capacity){

            $kg=$waste->getKg();
            
            foreach($this->services as $key => $service)
                if($service->WasteAccept($waste) && !$service->isClose())
                    $co2 += $service->WasteTreatment($waste);

            $this->capacity -= $kg;

        }

        return $co2;

    }

    /**
     * @param Waste $waste
     * @return bool
     */
    public function wasteAccept(Waste $waste) : bool{

        return true;

    }

}