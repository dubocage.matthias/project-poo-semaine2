<?php

namespace App\Entity;

use App\Entity\AbstractClass\Service;

class Incinerator extends Service{
    
    /**
     * @var array $TABCO2
     */
    private static $TABCO2 = ['PET' => 40 , 'PVC' => 38 , 'PC' => 42 , 'PEHD' => 35, 
     'organique' => 28, 'metaux' => 50 ,'verre' => 50,'papier' => 25, 'autre'=> 30];

     /**
      * @param float $capacity
      */
    public function __construct(float $capacity){

        $this->capacity = $capacity;

    }

    /**
     * @param Waste $waste
     * @return float
     */
    public function wasteTreatment(Waste $waste): float{
        
        $co2= 0.0 ;

        if($this->capacity >= $waste->getKg()){

            $co2 = $waste->getKg()*$this::$TABCO2[$waste->getType()];
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());

        }else{

            $co2 = $this->capacity*$this::$TABCO2[$waste->getType()];
            $waste->removeKg($this->capacity);
            $this->capacity = 0;

        }

        return $co2;
        
    }

    /**
     * @param Waste $waste
     * @return bool
     */
    public function wasteAccept(Waste $waste) : bool{

        return true;

    }
}